# xDAQ

[xDAQ](xdaq.web.cern.ch/) is a data acquisition framework widely used
throughout CMS.

In a complete system there are several components

-   `jobcontrol`: service which is able to stop and start xDAQ
    applications
-   `XaaS` : xDAQ as a service
-   `xmas` : xDAQ monitoring and alarming service
-   `hyperdaq` : web-based interface to xDAQ applications
-   subsystem specific xDAQ applications

How to interface with these components is describe in the linked pages.

## GEM xDAQ

Currently, the GEM xDAQ implementation is controlled through the
`GEMSupervisor` application. It controls and manages various
`<X>Manager` applications, as well as sending `SOAP` messages to the
`TCDS<gemos-expert-tcds-guide>`{.interpreted-text role="ref"}
applications.

Most of GEM xDAQ applications have an internal finite state machine
(FSM) defining the allowable states the application may be in, as well
as the allowed transitions into and out of a given state. Transtions
into each state have specific actions associate with them. If a
transition fails for one reason or another, an error message is logged
and the FSM should go to the `ERROR` state.

## xDAQ Services (`XaaS`)

### `jobcontrol`

The `jobcontrol` executable is able to start and stop `xdaq.exe`
executives on demand (it is used by RCMS). To do this, the `jobcontrol`
service needs to be running on the machine, and in the zone to which the
executive should belong. In GEM usage, the `jobcontrol` process is
running on port `39999`, and its hyperdaq page is accessible there.

From the hyperdaq page, any process started by `jobcontrol` can be
killed or its log viewed.

### `xmas`

`xdaq` monitoring and alarming service, or `xmas` is a monoitoring and
alarmaing framework provided with the xDAQ framework. It utilizes
\"monitorable\" items exposed through `InfoSpace` objects in the `xdaq`
applications and provides a mechanism to pulse the application and
request an update in the monitoring information. It is not currently
used in GEM applications.
