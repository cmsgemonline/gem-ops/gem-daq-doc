# Infrastructure

## Service account & e-groups

Whenever a user account is required, the common service account is used:

* `cmsgemonline`

Otherwise, memberhsips and authorizations are managed through various e-groups:

* `cms-gem-online`
* `cms-gem-online-admins`
* `cms-gem-online-b904int-users`
* `cms-gem-online-devs`
* `cms-gem-online-experts`
* `cms-gem-online-notifications`

While the usage of each of those groups should be clear based on their name, it is worth mentioning `cms-gem-online` as the main e-groups for general announcements and basic permissions.

## EOS storage

An EOS project space has been created to serve web pages, host software
repositories, and perform various other custodial tasks. The root of the
project space is:

```
/eos/project-c/cmsgemonline/
```

Notable locations are:

| Path               | Notes                                                        |
|--------------------|--------------------------------------------------------------|
| `gem-csc-trigger`  | Data shared with the `cms-gem-csc-trigger-taskforce` e-group |
| `public`           | Data shared with the `cms-gem-online` e-group                |
| `public/runs`      | Configuration information for Global runs                    |
| `www`              | Data shared with the `wwweos` account                        |
| `www/cmsgemonline` | [cmsgemonline](https://cmsgemonline.web.cern.ch/) website    |
| `www/cmsgemos`     | [cmsgemos](https://cmsgemos.web.cern.ch) website             |


## Network topology

### Local network

For local network setup for 904 labs, and recommended for teststands, several guidelines are provided.

- μTCA shelves and devices are on a `192.168.0.0/16` network.
  - MCHs (NAT, not VadaTech) are assigned IP addresses of `192.168.<shelf ID>.10`

!!! note
    The IP address assignment may need to be done manually, if the
    MCH was not previously configured by one of the GEM DAQ expert
    team.

- AMC13s use their hard-coded IP address assignments based on the device S/N

!!! note
    Assignment via the `sysmgr` is possible, but has not been reliably deployed

- CTP7s in the shelves assigned geographic IP addresses of the form `192.168.<shelf ID>.<40+slot ID>`

- Host aliases are set up in `/etc/hosts` to allow for name construction in software, e.g., `gem-shelf01-amc02`

### P5/904 technical network

The network toplogy for machines and devices connected to the P5 or 904
technical network is managed by the CMS sysadmins.

Physical machines are named by their rack/slot identifiers, virtual
machines based on their physical location.

The μTCA shelf identifier is given by the rack U-slot number
corresponding to the bottom of the shelf, and all devices therein
inherit this base hostname. For example, currently we have two μTCA
shelves installed at P5 in the rack `S2E01`. The first shelf was
installed with its bottom at `U23`, and the second was installed with
the bottom at `U14`. The `control_hub` (and consequently, the `sysmgr`)
machine is `ctrl-s2c17-22-01.cms`, aliased to `bridge-s2e01-23.cms`
(this is actually the same machine for both μTCA shelves, and there is
no corresponding `bridge-s2e01-14.cms` alias). The MCH for this shelf is
aliased to `mch-s2e01-23-01`. Each AMC13 has two network interfaces, one
for the T1 and the other for the T2, and these are aliased to
`amc-s2e01-23-13-t1` and `amc-s2e01-23-13-t2`. The other AMC cards in
the μTCA shelf will have aliases `amc-s2e01-23-XX`, were `XX` is the
slot number.

When registering a new AMC card at P5, a Jira ticket should be
opened with the sysadmins to tell them the "birdname", but the
geographic slot-based identifiers should already be registered if the
shelf was previously registered.

## FED mapping

The FED number (or ID) is an important configuration parameter required for proper analysis of the data taken.
The mapping between the FED ID and its usage is described in the table below:

| FED ID    | Usage              |
|-----------|--------------------|
| 1467      | GE-1/1             |
| 1468      | GE+1/1             |
| 1469      | GE2/1 demonstrator |
| 1470-1472 | GE2/1              |
| 1473-1476 | ME0                |
| 1477-1478 | Test setups        |
