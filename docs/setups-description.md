# Setups description

The supported GEM setups are split in the following systems:

=== "p5"

    This is the main production system in CMS.
    The operations are organized by GEM Run Coordinators and the Commissioning group.

    _It consists of the `gem-` machines in the `.cms` network._

=== "b904 integration"

    This system is composed of various development and integration setups in b904.
    Their usage is less constrained and described [below](#integration-setup-usage).

    _It consists of the `gem904-` machines in the `.cms904` network and the `gem904int-` machines in the GPN network._

=== "b904 QC"

    This system is dedicated to the QC7 and QC8 tests of the production detectors in b904.
    The operations are organized by the production group.

    _It consists of the `gem904qc-` machines in the GPN network._

## Connection

### Account

=== "p5"

    If you do not have an account on the `.cms` technical network, please contact the GEM responsibles as listed [here](https://twiki.cern.ch/twiki/bin/viewauth/CMS/ClusterUsersGuide#Getting_an_account_or_access_to) or the GEM Run Coordinators.
    Your NICE username is required to complete the account creation.

    Once your acount will be created, you will receive a confirmation on your CERN email address with the credential. They can be changed at any moment with the `passwd` command on `cmsusr`.

    !!! note
        The account will be created simultaneously for both the p5 (`.cms`) and 904 (`.cms904`) networks, you do not need to request an account for each.

### SSH connection

Some ressources are accessible only from the CERN GPN or from technical networks which themselves must be reached from within the GPN.
In order to access those ressources tunneling though the right network and machines is required.
Below are several helpful snippets to include in your `ssh` configuration (`~/.ssh/config` by default) file to help you setting up your system.

Additional options can be used to facilite the usage (such as multiplexed connections), but can be tricky to configure.

=== "p5"

    ```
    Host cern-tunnel
        Hostname lxplus.cern.ch
        User <nice-username>
        # IdentityFile <lxplus-enabled-ssh-key>

    Host cmsusr.cern.ch cmsusr cmsusr-tunnel
        Hostname cmsusr.cern.ch
        User <nice-username>

    # Only cmsusr-tunnel opens the tunnel
    Host cmsusr-tunnel
        # SOCKS5 proxy
        DynamicForward localhost:5000
        # X2GO server
        LocalForward 5011 cc7x2go01.cms:22
        LocalForward 5012 cc7x2go02.cms:22
        LocalForward 5013 cc7x2go03.cms:22
        LocalForward 5014 cc7x2go04.cms:22
        LocalForward 5015 cc7x2go05.cms:22
        LocalForward 5016 cc7x2go06.cms:22

    # Hop through lxplus only if not in GPN
    Match host cmsusr.cern.ch !exec "grep -q '^search cern\.ch$' /etc/resolv.conf"
        ProxyJump cern-tunnel

    # Direct connection to the
    Host *.cms
        ProxyJump cmsusr
        # IdentityFile <cmsusr-enabled-ssh-key>
        RequestTTY yes
    ```

    More details and updated information can be found on the [CMS Cluster TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/ClusterUsersGuide#How_to_tune_your_OpenSSH_configu).

!!! important
    The above snippet uses the `ProxyJump` option of `openSSH`.
    It is availble in versions greater than 7.3.
    If you are running a verision older than this, you should replace the `ProxyJump` statements with the appropriate option from the list below:

    - `ProxyJump <host>` for OpenSSH 7.3 and up
    - `ProxyCommand ssh <host> -W %h:%p` for OpenSSH 5.4 and up
    - `ProxyCommand ssh <host> nc %h <ssh port on server>` for all others

### Web proxy

Using a proxy auto-connect (PAC) file will allow you to set up tunnels such
that you can easily access web resources on the CMS network or GPN from outside
the respective network. If you do this, you won't have to individually forward
specific ports and then access them via `localhost:<port>`. Instead you simply
point your browser to the real URL and the proxy you set up with the SSH
dynamic port forwarding (a.k.a. SOCKS proxy) ensures that the request makes it
to the correct machine. The file [located here](p5.pac) will assume that you
have set up the ports as in the examples above. If not, you should ensure that
the port forwards you use match the rules in the PAC file.

Enabling this in `firefox` is done via the "Preferences" menu. Inside of
"Preferences" menu is an area for "Network settings" click on the "Settings..."
button and click on "Automatic proxy configuration URL". In the text box you
can you can specify the path to a local copy of the PAC file on your computer
with `file:///path/to/pac/file`. Using a direct link the the PAC file may also
work, but is **not recommended** since the provided URL may not be stable over
time.

To enable on a browser that is not Firefox, look up instructions for how to add
an "Automatic proxy configuration URL" and add the path to the PAC file there.
It can also be enabled system wide on your Linux machine, but the setup is
different depending on your exact configuration. Setup on Mac can be done
either through the browser or through the network settings. Setup on Windows is
an exercise left for the reader.

!!! important
    In order to use the proxy, first enable an SSH tunnel:

    ``` bash
    ssh cmsusr-tunnel
    ```


??? info
    The following regular expression can be used in proxy extensions (e.g. `FoxyProxy`) instead of a PAC file.

    * p5: `^(?:[^:@/]+(?::[^@/]+)?@)?(?:[\w-]+\.)*(?:cms)(?::\d+)?$`

### DCS

=== "p5"

    You can use the RDP (_Remote Desktop Client_) client of your choice with [this cerntscms configuration file](cerntscms.cern.ch.rdp). No additional tunnel is required.

    On Linux, you can use `xfreerdp` from the command line:

    ``` bash
    xfreerdp <path-to-the-rdp-file> /size:1918x1060 /gu:CERN\\<nice-username> /u:CERN\\<nice-username>
    ```

### Old nodes access

Newer Linux releases (e.g. AlmaLinux 9) disable the old and insecure algorithms
or protocols by default. While technically a good thing, this newer default
prevents connecting to older nodes, such as SYx525 CAEN mainframes or non
patched CTP7. In such cases, you get errors such as:

* `ssh_dispatch_run_fatal: Connection to ww.xx.yy.zz port 22: error in libcrypto`
* `Unable to negotiate with ww.xx.yy.zz port 22: no matching host key type found. Their offer: ssh-dss`

On the P5/CMS system, a dedicated node allowing weaker algorithms has been
created: `access-old-nodes`. In order to make use of it, first connect directly
to the node (using the `ProxyJump` directive is not sufficient; the connection
must be _initiated_ from that specific node). From there, connect via SSH as
usual.

Some target may however require additional configuration options. Typically,
the SYx525 CAEN mainframes require the addition of
`-oPubkeyAcceptedAlgorithms=+ssh-dss -o HostKeyAlgorithms=+ssh-dss` (can also
be added in the `.ssh/config` file).

No such node or specific configuration instructions currently exist for other
setups.

## Integration setup usage

It is important to realize we are all sharing a set of common equipment and are working towards a common goal.
Furthermore you and your coworkers may have different skill levels and possess varying degrees of familiarity with the hardware and the software.
This brings us to the first important point:

!!! warning
    **Leave the setup in a working state**.

    This specifically refers to the setup infrastructure (e.g., fiber patch panels, power supplies, DAQ computer, etc...) and electronics.

It can be expected that you would need to configure the back-end and/or front-end electronics for whatever test/development you are working on.
But the setup should *always* be left in a usable state for the next user.

Moreover, if you are not on the list of approved individuals who can modify the hardware/stand infrastructure you should not (if you are wondering if you are on this list it probably means you are *not*).
Failure to follow these rules makes more work for the DAQ expert team, setup responsible/sysadmin.
