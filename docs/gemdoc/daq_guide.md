# A GEM DOC's DAQ Guide 

This guide is to help walk new docs through basic procedures at p5 concerning the DAQ. 

## Calibration Scans 

For scans, you must be in local DAQ -- you coordinate with the Shift Leader when this is possible. And when you return to global, you must **DESTROY** the GEM/local or minidaq session. Instructions on how to take the scans are already given here : (link) .

General information about the daily scans : 

1. **Calibration Pulse Scan** – determines how many dead channels we have 
	* uses GEM Local so can be taken without MiniDAQ
    * this scan takes only a few seconds 


2. **SBit** - determines the level of noise (Need STANDBY & INDUCTION OFF)
	* uses GEM Local so can be taken without MiniDAQ
    * this scan takes around 5 minutes per iteration. It is common to set iterations = 1. 


3. **Low Threshold Run (Local Run)** — helps to keep track of noisy channels ? 
	* uses MiniDAQ 
    * Usually have this running ~20-30min  before stopping the run 

For all scans (except SBit), it does not matter what the HV condition is (unless a specific request demands a certain configuration) -- just make sure to document it. HV can be standby or ON, but *LV must be on*. 

**If LV is OFF -- you can't read anything out!!**

> Laurent's Request : If you encounter something out of the ordiniary in the DAQ like say a scan didn't run as usual and had to be retaken -- you open an e-log under the Subsystems > GEM > P5 > DAQ category with the type *Problem Report*. This is to be done even if you are able to find a way around the problem -- we need to keep track of all the small glitches to make sure we don't miss anything major. 

--- 

## Taking Scans during InterFill 

It is possible to complete all scans during a standard interfill. Here are a few tips to take these scans efficiently.

**Beam Dump**
You can have an agreement with the shift leader to immediently place you in local DAQ and DCS after beam dump (and call you after it has been done). This saves you a couple of minutes as you have given your consent beforehand but you must be ready on your end. 

1. Take the Calibration Scan (do not analyze until later) 
2. Prepare the system for the Sbit Scan and take it 

These two scans fit nicely in the time it takes for **Ramp Down**. 

3. Return the system to HV Stanby (after the sbit) 

4. By **No Beam, SetUp**, you should be taking the low threshold Run. (HV at standby or ON depending on request)


At **Injection Probe Beam, Injection Setup Beam**, the LHC Protection should fire and if you aren't in Standby, it will apply Standby for you. You should have enough statistics (~20min) to return to global DAQ and central DCS before Injection Setup Beam ends-- making sure to destroy the minidaq before you do so!! 


---

## Output of Runs 

To check the status of the runs and make sure everything was applied correctly : 

1. To check a Low Thr Run :
    
        ssh username@cmsusr 
        ssh gem-daq01 
        cd /gemdata/runs/minidaq/<run_number>
        ##open any of the fed config info json files to double check thr=1 has been applied 

    You'll be able to open the output config files of these runs immediately following the start of the run, but recall that the data is sent to Tier0. 

    Additionally, you can check the monitoring page : [XDAQ](http://srv-s2g18-33-01.cms:20300/urn:xdaq-application:lid=61468 ) 
    >L1A Rate should NOT be 0  


2. To check a Calibration Scan (Sbit, CalPulse, etc.) : 
        
        ssh username@cmsusr
        ssh gem-daq01
        cd /gemdata/log/
        ls -lrth

        ## pick out the most recent log (it should be 20400 or 20200 not the 20300 …) 
        tail -n 50 gem-daq01-20400-2022-06-12T18:33:32.723500Z-4258.log 
        ## the other option is to use the grep command to search the file for ‘scan done’

        ## the location of the scan outputs are: 
        /gemdata/runs/local/run#

A scan will be DONE when all 13 AMC Slots show DONE (6 for GE+1/1 and GE-1/1 and 1 for GE21 demo)

--- 

## Analyze the Calibration Pulse Scan 
(to be done in /gemdata directory)
        
        ssh gemvm-control 
        sudo -u gemdoc -i 
        gem-find-dead-channel -r <RUNNUMBER>

After above is complete, open [Grafana : Dead Channels Trends](https://cmsgemonline-monitoring.app.cern.ch/d/Trh6MZd4k/dead-channels-trends?orgId=1&from=now-7d&to=now) and refresh page. Look through and see if any SC had a spike in # of dead channels. (Can also check the nice overview at the top of the page before scrolling through all SC). 
It is also recommended to have a range of at least 7 days. 

Note that some spikes are not taken as seriously as others : 

* some SC fluctuate from all OK to all VFAT channels (128) DEAD 
* Sometimes datapoints are not reported for a SC 
* Sometimes you have a small fluctuation (≤ 2 dead channels) being recovered or discovered 
> e.g. GE11-P-36 Ly2 fluctuates a lot 

In all cases, make sure to document in your elog.


**Reporting Scans** 
Like mentioned in the overview guide, you must write an elog for the calibration scans taken and also update the google doc with the local run taken. 

--- 

## Issues and Fixes 
There are some known issues that central shifters may call you about. Time is of the essence, but it is better to understand 100% the problem before hanging up to accurately report information. 

Here is how to handle known situations : 

### FEDs stuck in WARNING 
> The error : GEM FED #### is stuck in TTS WARNING 
    
Central Trigger shifter will have instructions, but just in case : 

“As of July 18, there is an issue where the GEM FEDs get stuck in global. Tell the Shift leader to issue a *“TTC resync”* and see if this fixes the problem before doing red or green recycling. A TTC Resync takes about ~5 seconds. 

Please note that “red recycle” and “green recycle” are simply ‘quick’ fixes to be used with care. If there are no collisions and it is during normal working hours, please call the DAQ expert in case of an error in global to allow them to investigate.”

If the error is : 
>TTS BLOCKED 

Most likely, the AMC13 needs to be rebooted. **CALL DAQ EXPERT** 


### Too Few OHs are ON 
If you receive a notification or call that too few OHs are ON, this implies too few chambers have LV ON. We will be seen in Central DCS as “Not Ready”. 

Simply request for a GR when next possible if GEM is in global. 

* This can be in the shadow of another detector if stable beams are ongoing. Do NOT have central crew reconfigure us as this will cause deadtime to be assigned to us. It takes ~ 35 seconds to configure GEMs. 

If GEM is in local, re-configure in either GEM-local or MiniDAQ. (It is assumed that if we are in local, there are no stable beams.)



### GE21 Demonstrator (FED 1469)
If anything is reported to you concerning the GE21 demonstrator during stable beams, you must request to take the FED out of the Fill. 
> In the past there were instances of *High Rates of Deadtime* which can be solved by a GR but this is a quick fix and it is not guaranteed that the issue will not occur again.  

